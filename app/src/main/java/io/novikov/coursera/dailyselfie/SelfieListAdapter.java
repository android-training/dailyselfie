package io.novikov.coursera.dailyselfie;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.FileObserver;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.graphics.Bitmap;
import android.widget.Toast;

import java.io.File;

public class SelfieListAdapter extends ArrayAdapter<SelfieItem> {


    private static final String TAG = "SelfieListAdapter";

    private FileObserver fileObserver;

    public SelfieListAdapter(Context context, int resID, File storageDir) {
        super(context, resID);

        //Read existing files
        for( File file : storageDir.listFiles()) {
            add(file.getPath());
        }
/*
        //Watch storage directory for update
        fileObserver = new FileObserver(storageDir.getAbsolutePath(), FileObserver.CREATE) {
            @Override
            public void onEvent(int event, String path) {
                Log.d(TAG, "New FO event " + event + " on path : " + path);
                if(event == FileObserver.CREATE) add(path);
            }
        };
        fileObserver.startWatching();
*/
    }

    public boolean add(String path) {
        try{
            add(new SelfieItem(getContext(), this, path));
        } catch(Exception e) {
            Log.i(TAG, "Failed to add selfie by path " + path + " because of exception :" + e);
            e.printStackTrace();
            //Toast.makeText(SelfieListAdapter.this.getContext(), R.string.toast_failed_to_load_selfie, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

	static class ViewHolder {
        TextView compoundView;
	};

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final SelfieItem selfieItem = (SelfieItem)getItem(position);

        ViewHolder viewHolder;
		TextView itemLayout = (TextView) convertView;
		if (itemLayout==null) {

			itemLayout = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.selfie_item, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.compoundView = (TextView) itemLayout.findViewById(R.id.photo_list_compound_view);

			itemLayout.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		// Fill in specific data
		// Remember that the data that goes in this View
		// corresponds to the user interface elements defined
		// in the layout file



		final TextView compoundView = viewHolder.compoundView;
		compoundView.setText(SelfieItem.FORMAT.format(selfieItem.getDate()));
        final Bitmap bm = selfieItem.getThumb();
        final Drawable d = new BitmapDrawable(getContext().getResources(), bm);
        d.setBounds(0, 0, bm.getWidth(), bm.getHeight());
        compoundView.setCompoundDrawables(d, null, null, null);

		// Return the View you just created
		return itemLayout;

	}

}
