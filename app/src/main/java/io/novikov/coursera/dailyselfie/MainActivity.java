package io.novikov.coursera.dailyselfie;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.os.SystemClock;
import android.util.Log;
import android.os.Environment;
import android.os.Bundle;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.provider.MediaStore;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    final static private String TAG = "MainActivity";
    public static final long INITIAL_ALARM_INTERVAL = 2 * 60 * 1000L; //2 minutes
    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";
    private static final DateFormat dateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);

    private final File mStorageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
    private SelfieListAdapter mAdapter;
    private String mCurrentPhotoPath;

    public boolean takePicture(MenuItem item){
       Log.d(TAG, "takePicture called from " + item);
       try { dispatchTakePictureIntent(); }
       catch ( Exception e) {
           Toast.makeText(getApplicationContext(), R.string.toast_camera_error, Toast.LENGTH_SHORT).show();
           Log.e (TAG, "Exception caught" + e);
           e.printStackTrace();
           return false;
       }

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ListView photoListView = (ListView) findViewById(R.id.photo_list_view);
        photoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "photoListView item was clicked: " + position + "  " + id);
                SelfieItem si = (SelfieItem) parent.getItemAtPosition(position);
                String path = si.getPath();
                Intent intent = new Intent(MainActivity.this, DisplayImageActivity.class);
                intent.putExtra(getString(R.string.extra_image_path), path);
                startActivity(intent);
            }
        });

        if (!mStorageDir.isDirectory()) {
            if (mStorageDir.mkdirs())
                Toast.makeText(getApplicationContext(), R.string.toast_new_storage_directory_created, Toast.LENGTH_LONG).show();
            else {
                DialogFragment dialog = new UnableToCreateStorageDialogFragment();
                dialog.show(getFragmentManager(), "Failed to create storage dialog");
            }
        }
        Log.i(TAG, "Storage directory : " + mStorageDir.getAbsolutePath());

        setRepeatedAlarmNotification(INITIAL_ALARM_INTERVAL);

        mAdapter = new SelfieListAdapter(getApplicationContext(), R.id.photo_list_compound_view, mStorageDir);
        photoListView.setAdapter(mAdapter);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.shot) {
            return takePicture(item);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        boolean result = false;

        if (id == R.id.nav_delete_all) {
            new DeleteAllDialogFragment().show(getFragmentManager(), "Clear all dialog");
        } else if (id == R.id.nav_alarm_settings) {
            new AlarmSettingsDialogFragment().show(getFragmentManager(), "Alarm Settings");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return result;
    }

    private void dispatchTakePictureIntent() {
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Log.i(TAG, "Device has no camera");
            Toast.makeText(getApplicationContext(), R.string.toast_no_camera, Toast.LENGTH_SHORT).show();
        }

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(TAG, "Failed to create file: " + ex);
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                mCurrentPhotoPath = photoFile.getAbsolutePath();
            }
        } else {
            Log.e(TAG, "resolveActivity returned null");
        }
    }

    	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.i(TAG, "Entered onActivityResult() for request " + requestCode + " with result " + requestCode);

		if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK)
		{
			mAdapter.add(mCurrentPhotoPath);
            mCurrentPhotoPath = null;
		}
	}

    public boolean setRepeatedAlarmNotification(long interval) {
        try {
            // Get the AlarmManager Service
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

            // Create an Intent to broadcast to the AlarmNotificationReceiver
            Intent notificationReceiverIntent = new Intent(MainActivity.this,
                    AlarmNotificationReceiver.class);

            // Create an PendingIntent that holds the NotificationReceiverIntent
            PendingIntent notificationReceiverPendingIntent = PendingIntent.getBroadcast(
                    MainActivity.this, 0, notificationReceiverIntent, 0);


            if (interval > 0) // Set repeating alarm
                alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME,
                        SystemClock.elapsedRealtime() + interval, interval,
                        notificationReceiverPendingIntent);
            else //cancel alarm
                alarmManager.cancel(notificationReceiverPendingIntent);
        } catch ( Exception e ) {
            Log.e (TAG, "Failed to set up alarm because of exeption:" + e);
            return false;
        }
        Log.d(TAG, "Alarm notification set with interval " + interval + " ms");
        return true;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = dateFormat.format(new Date());
        String imageFileName = timeStamp + ".jpg";
        File image = new File(mStorageDir, imageFileName);
        Log.d(TAG, "New image file path: " + image.getAbsolutePath());

        return image;
    }

    private boolean clearStorage() {
        mAdapter.clear();
        try {
            if (mStorageDir.isDirectory()) {
                String[] children = mStorageDir.list();
                for (String fn : children) {
                    if (!new File(mStorageDir, fn).delete())
                        return false;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to clean storage due to exception " + e);
            return false;
        }
        Log.i(TAG, "Local storage is clear");
        return true;
    }


    public static Date getTimeStamp(String filename) {
        try {
            return dateFormat.parse(filename);
        } catch (ParseException e) {
            Log.i (TAG, "Failed to extract timestamp from filename :" + filename
                    + " because of exception : " + e);
            return new Date(0);
        }
    }

    static public class UnableToCreateStorageDialogFragment extends DialogFragment {

        private static final String TAG = "UnableToCreateStorageDlg";
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.dialog_error_title)
                    .setMessage(R.string.dialog_failed_to_create_storage_message)
                    .setPositiveButton(R.string.button_report_issue, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d(TAG, "\"Report issue\" button clicked");
                            //TODO Report issue
                        }
                    })
                    .setNeutralButton(R.string.button_exit, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d(TAG, "\"Exit\" button clicked");
                            getActivity().finish();
                        }
                    })
                    .setNegativeButton(R.string.button_continue, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d(TAG, "\"Continue\" button clicked");
                            // Do nothing
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }

    public class AlarmSettingsDialogFragment extends DialogFragment {
        private static final String TAG = "AlarmSettingsDlg";
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.dialog_alarm_settings_title)
                    .setMessage(R.string.dialog_alarm_settings_message)
                    .setPositiveButton(R.string.button_every_2_minutes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                          setRepeatedAlarmNotification(MainActivity.INITIAL_ALARM_INTERVAL);
                        }
                    })
                    .setNeutralButton(R.string.button_every_day, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            setRepeatedAlarmNotification(AlarmManager.INTERVAL_DAY);
                        }
                    })
                    .setNegativeButton(R.string.button_never, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            setRepeatedAlarmNotification(0);
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }
    public class DeleteAllDialogFragment extends DialogFragment {
        private static final String TAG = "DeleteAllDlg";
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.dialog_delete_all_title)
                    .setMessage(R.string.dialog_delete_all_message)
                    .setPositiveButton(R.string.button_delete_all, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            clearStorage();
                        }
                    })
                    .setNeutralButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }

}

