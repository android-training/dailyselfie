package io.novikov.coursera.dailyselfie;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.BaseAdapter;

/**
 * Created by al on 11/18/15.
 */
public class SelfieItem {
    private static final String TAG = "SelfieItem";
    public final static SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

    public static final int THUMB_HEIGHT = 100;
    private static Bitmap thumbPlaceholder = null;

    private Bitmap mThumb;
    private Date mDate;

    private Uri mUri;
    private BaseAdapter mAdapter;

    SelfieItem(Context context, BaseAdapter adapter, String path) {
        if (thumbPlaceholder == null) thumbPlaceholder = makeThumbPlaceholder(context);
        mAdapter = adapter;
        mUri = Uri.parse(path);
        mThumb = thumbPlaceholder;
        mDate = MainActivity.getTimeStamp(mUri.getLastPathSegment());
        //new makeThumbAsyncTask(mThumb).execute(path);
        mThumb = makeThumb(path);
    }

    public Bitmap getThumb() {
        return mThumb;
    }

    public Date getDate() {
        return mDate;
    }

    public String getPath() {
        return mUri.getPath();
    }

    static private Bitmap makeThumb(String path)
    {
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        // Determine how much to scale down the image
        bmOptions.inSampleSize = bmOptions.outHeight/THUMB_HEIGHT;
        bmOptions.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, bmOptions);
    }

    private Bitmap makeThumbPlaceholder(Context context) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize = 2;
        return BitmapFactory.decodeResource(context.getResources(), R.drawable.kedi, bmOptions);
    }

    class makeThumbAsyncTask extends AsyncTask<String, Void, Bitmap> {
        Bitmap bitmap;
        public makeThumbAsyncTask(Bitmap bt) {
            bitmap = bt;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Log.d(TAG, "Background decoding started for file :" + params[0]);
            try {
                return makeThumb(params[0]);
            } catch (Exception e) {
                Log.e(TAG, "Failed to decode file " + params[0] + " because of exception " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bt) {
            super.onPostExecute(bt);
            bitmap = bt;
            mAdapter.notifyDataSetChanged();
        }

    }
}